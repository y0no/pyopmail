# -*- coding: utf-8 -*-

import requests
from os.path import join
from bs4 import BeautifulSoup as bs4
from bs4 import Comment

class PYopmail(object):
    def __init__(self, username=None, url='http://www.yopmail.com', lang='en', version='2.4'):
        self.url = url
        self.lang = lang
        self.version = version
        self.username = username
        self.session = requests.session()

    def list_mails(self):
        payload = {'login': self.username, 'v': self.version}
        r = self.session.get(join(self.url, self.lang, 'inbox.php'), params=payload)

        return self._parse_list_mails(r.content)

    # To modify to catch title and date
    def _parse_list_mails(self, source):
        html = bs4(source)

        messages = html.find_all('a', class_='lm')

        return [self._parse_elem(i) for i in messages]

    def _parse_elem(self, elem):
        return elem['href'].replace('r=1&', '').replace('mail.php?id=', '')

    def get_mail(self, mail_id):
        payload = {'id': mail_id}
        r = self.session.get(join(self.url, self.lang, 'mail.php'), params=payload)

        return self._parse_get_mail(r.content)

    def _parse_get_mail(self, source):
        html = bs4(source)
        
        message = html.find('div', id='mailmillieu')

        message.script.decompose()
        message.span.decompose()

        #spans = message.find_all('span')
        #if spans is not None:
        #    [span.decompose() for span in spans[-1]]

        return message

    def read_mail(self, mail_id):
        return self.get_mail(mail_id).get_text()

    def click_mail(self, mail_id, limit=None):
        links = get_links(mail_id)

        if links is not None:
            return [self.session.get(link) for link in links[:limit]]

    def click_last_mail(self, limit=None):
        links = self.get_last_mail_links()

        if links is not None:
            return [self.session.get(link) for link in links[:limit]]

    def get_links(self, mail_id):
        message = self.get_mail(mail_id)

        links = message.find_all('a')
        if links is not None:
            return [link.get('href') for link in links]

    def get_last_mail_links(self):
        last_mail_id = self.list_mails()[0]

        return self.get_links(last_mail_id)
